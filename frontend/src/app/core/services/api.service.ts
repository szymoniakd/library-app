import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private readonly apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  get(url: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}${url}`);
  }

  delete(url: string): Observable<any> {
    return this.http.delete<any>(`${this.apiUrl}${url}`);
  }

  put(url: string, data: object): Observable<any> {
    return this.http.put<any>(`${this.apiUrl}${url}`, data);
  }

  post(url: string, data: object): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}${url}`, data);
  }
}
