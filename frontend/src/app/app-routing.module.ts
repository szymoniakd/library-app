import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BooksComponent} from './books/books.component';
import {BookDetailsComponent} from './books/book-details/book-details.component';
import {BookFormComponent} from './books/book-form/book-form.component';
import {CategoriesComponent} from './categories/categories.component';
import {CategoryDetailsComponent} from './categories/category-details/category-details.component';
import {CategoryFormComponent} from './categories/category-form/category-form.component';

const routes: Routes = [
  { path: '', redirectTo: 'books', pathMatch: 'full' },
  { path: 'books', component: BooksComponent },
  { path: 'books/:id', component: BookDetailsComponent },
  { path: 'books/:id/edit', component: BookDetailsComponent, data: { edit: true } },
  { path: 'book/new', component: BookFormComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'categories/:id', component: CategoryDetailsComponent },
  { path: 'categories/:id/edit', component: CategoryDetailsComponent, data: { edit: true } },
  { path: 'category/new', component: CategoryFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
