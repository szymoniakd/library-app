import { Injectable } from '@angular/core';
import {ApiService} from '../core/services/api.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Category} from './category';
import {CategoryDetails} from './category-details/CategoryDetails';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private url = '/category';

  constructor(private apiService: ApiService) { }

  getCategories(): Observable<Category[]> {
    return this.apiService.get(this.url).pipe(map(response => response.results));
  }

  getCategory(id: number): Observable<CategoryDetails> {
    return this.apiService.get(`${this.url}/${id}`);
  }

  deleteCategory(id: number): Observable<any> {
    return this.apiService.delete(`${this.url}/${id}`);
  }

  saveCategory(id: number, category: Category): Observable<any> {
    return this.apiService.put(`${this.url}/${id}`, category);
  }

  createCategory(category: Category): Observable<any> {
    return this.apiService.post(`${this.url}`, category);
  }
}
