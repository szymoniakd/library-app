import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {CategoriesService} from '../categories.service';
import {CategoryDetails} from '../category-details/CategoryDetails';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.less']
})
export class CategoryFormComponent implements OnInit {
  @Input() category: CategoryDetails;
  categoryForm = this.formBuilder.group({
    name: ['', Validators.required],
  });

  constructor(
    private formBuilder: FormBuilder,
    private location: Location,
    private categoriesService: CategoriesService,
    ) { }

  ngOnInit(): void {
    const defaultCategory: CategoryDetails = { id: null, name: '', books: [] };
    this.category = this.category || defaultCategory;
    this.categoryForm.patchValue(this.category);
  }

  onSubmit(): void {
    if (this.category.id) {
      this.categoriesService.saveCategory(this.category.id, this.categoryForm.value).subscribe(() => this.goBack());
    } else {
      this.categoriesService.createCategory(this.categoryForm.value).subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }
}
