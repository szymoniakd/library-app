import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import {CategoriesService} from '../categories.service';
import {CategoryDetails} from './CategoryDetails';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.less']
})
export class CategoryDetailsComponent implements OnInit {
  id: number;
  category: CategoryDetails;
  edit: boolean;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private categoryService: CategoriesService,
    ) {
    this.edit = false;
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.edit = data.edit;
    });
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getCategory();
  }

  goBack(): void {
    this.location.back();
  }

  delete(): void {
    this.categoryService.deleteCategory(this.id).subscribe(() => this.goBack());
  }

  getCategory(): void {
    this.categoryService.getCategory(this.id).subscribe(category => this.category = category);
  }
}
