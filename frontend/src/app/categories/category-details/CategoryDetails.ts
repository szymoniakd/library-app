import {Book} from '../../books/book';

export interface CategoryDetails {
  id: number;
  name: string;
  books: Book[];
}
