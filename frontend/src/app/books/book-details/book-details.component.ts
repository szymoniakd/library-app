import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import {BookDetails} from './BookDetails';
import {BooksService} from '../books.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.less']
})
export class BookDetailsComponent implements OnInit {
  id: number;
  book: BookDetails;
  edit: boolean;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private booksService: BooksService,
  ) {
    this.edit = false;
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.edit = data.edit;
    });
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getBook();
  }

  goBack(): void {
    this.location.back();
  }

  delete(): void {
    this.booksService.deleteBook(this.id).subscribe(() => this.goBack());
  }

  getBook(): void {
    this.booksService.getBook(this.id).subscribe(book => this.book = book);
  }
}
