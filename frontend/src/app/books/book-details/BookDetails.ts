import {Category} from '../../categories/category';

export interface BookDetails {
  id: number;
  title: string;
  categories: Category[];
}
