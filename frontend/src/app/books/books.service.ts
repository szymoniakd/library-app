import { Injectable } from '@angular/core';
import {ApiService} from '../core/services/api.service';
import {Observable} from 'rxjs';
import {Book} from './book';
import {map} from 'rxjs/operators';
import {BookDetails} from './book-details/BookDetails';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  private url = '/book';

  constructor(private apiService: ApiService) { }

  getBooks(): Observable<Book[]> {
    return this.apiService.get(this.url).pipe(map(response => response.results));
  }

  getBook(id: number): Observable<BookDetails> {
    return this.apiService.get(`${this.url}/${id}`);
  }

  deleteBook(id: number): Observable<any> {
    return this.apiService.delete(`${this.url}/${id}`);
  }

  saveBook(id: number, book: BookDetails): Observable<any> {
    return this.apiService.put(`${this.url}/${id}`, BooksService.processBook(book));
  }

  createBook(book: BookDetails): Observable<any> {
    return this.apiService.post(`${this.url}`, BooksService.processBook(book));
  }

  private static processBook(book: BookDetails): object {
    return {
    ...book,
      categories: book.categories.map(category => category.id),
    };
  }
}
