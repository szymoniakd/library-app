import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {BookDetails} from '../book-details/BookDetails';
import {BooksService} from '../books.service';
import {Location} from '@angular/common';
import {CategoriesService} from '../../categories/categories.service';
import {Category} from '../../categories/category';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.less']
})
export class BookFormComponent implements OnInit {
  @Input() book: BookDetails;
  bookForm = this.formBuilder.group({
    title: ['', Validators.required],
    categories: [],
  });
  availableCategories: Category[];
  dropdownSettings = {};

  constructor(
    private formBuilder: FormBuilder,
    private booksService: BooksService,
    private categoriesService: CategoriesService,
    private location: Location,
  ) { }

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


    const defaultBook: BookDetails = { id: null, title: '', categories: [] };
    this.book = this.book || defaultBook;
    this.bookForm.patchValue(this.book);
    this.categoriesService.getCategories().subscribe(categories => this.availableCategories = categories);
  }

  onSubmit(): void {
    if (this.book.id) {
      this.booksService.saveBook(this.book.id, this.bookForm.value).subscribe(() => this.goBack());
    } else {
      this.booksService.createBook(this.bookForm.value).subscribe(() => this.goBack());
    }
  }

  goBack(): void {
    this.location.back();
  }
}
