<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\MessageBag;
use Throwable;

class RequestValidationException extends Exception
{
    private $validationErrors = [];

    public function __construct(MessageBag $errors, $code = 400, Throwable $previous = null)
    {
        $this->validationErrors = $errors;

        parent::__construct('Provided data failed to pass validation.', $code, $previous);
    }

    /**
     * @return MessageBag
     */
    public function getValidationErrors(): MessageBag
    {
        return $this->validationErrors;
    }
}
