<?php

namespace App\Console\Commands;

use App\Console\Models\DecoratedClass;
use App\Console\Models\Node;
use App\Console\Models\NodeCollection;
use App\Console\Models\NodeList;
use App\Console\Models\Nodes;
use App\Console\Models\DecoratorClass;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ListStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:structure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = $this->getCsvData();

        $objects = [];

        foreach ($data as $row) {
            foreach ($row as $name) {
                if ($name) {
                    $letters = explode('.', $name);
                    $object = new DecoratedClass();
                    foreach ($letters as $letter) {
                        $object = new DecoratorClass($letter, $object);
                    }
                    $objects[] = $object;
                }
            }
        }

        foreach ($objects as $object) {
            $object->print();
        }

        return 0;
    }

    private function getCsvData(): array
    {
        $filePath = Storage::disk('local')->path('zadanie_rekrutacyjne.csv');
        $data = array_map(function ($data) {
            return str_getcsv($data, ';');
        }, file($filePath));
        unset($data[0]);
        return array_values($data);
    }
}
