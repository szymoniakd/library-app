<?php

namespace App\Console\Models;

interface DecoratorContract
{
    public function getName($letters);

    public function getSpaces($spaces);
}
