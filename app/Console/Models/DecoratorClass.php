<?php

namespace App\Console\Models;

class DecoratorClass implements DecoratorContract
{
    public $letter;

    public $object;

    public function __construct(string $letter = null, DecoratorContract $object = null)
    {
        $this->letter = $letter;
        $this->object = $object;
    }

    public function getName($letters = null)
    {
        if ($letters) {
            return $this->object->getName($this->letter . '.' . $letters);
        } else {
            return $this->object->getName($this->letter);
        }
    }

    public function getSpaces($spaces = null)
    {
        if ($spaces) {
            $added = '';
            $i = 0;
            while ($i <= strlen($this->getName())) {
                $added .= ' ';
                $i++;
            }
            return $this->object->getSpaces($added . $spaces);
        } else {
            return $this->object->getSpaces(' ');
        }
    }

    public function print()
    {
        echo $this->getSpaces();
        echo $this->getName();
        echo PHP_EOL;
    }
}
