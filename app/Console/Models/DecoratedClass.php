<?php

namespace App\Console\Models;

class DecoratedClass implements DecoratorContract
{
    public function getName($letters)
    {
        return $letters;
    }

    public function getSpaces($spaces)
    {
        return $spaces;
    }
}
