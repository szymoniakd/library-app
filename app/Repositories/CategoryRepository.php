<?php

namespace App\Repositories;

use App\Exceptions\ResourceNotFoundException;
use App\Http\Requests\CategoryRequest;
use App\Models\BaseModel;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Throwable;

class CategoryRepository extends AbstractRepository
{
    /**
     * @param int $id
     * @return Category
     * @throws ResourceNotFoundException
     */
    public function getById(int $id): BaseModel
    {
        $category = $this->getQuery()->with('books')->where('id', $id)->first();
        return $this->returnOrThrowOnNull($category, $id);
    }

    function getQuery(): Builder
    {
        return Category::query();
    }

    /**
     * @param CategoryRequest $categoryRequest
     * @param int|null $id
     * @return Category
     * @throws ResourceNotFoundException
     * @throws Throwable
     */
    public function save(CategoryRequest $categoryRequest, int $id = null): BaseModel
    {
        if ($id) {
            $category = $this->getById($id);
        } else {
            $category = new Category();
        }
        $category->name = $categoryRequest->name;
        $category->saveOrFail();
        return $category;
    }
}
