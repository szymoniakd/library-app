<?php

namespace App\Repositories;

use App\Exceptions\ResourceNotFoundException;
use App\Models\BaseModel;
use App\Repositories\Interfaces\RepositoryInterface;
use Exception;
use Illuminate\Database\Eloquent\Builder;

abstract class AbstractRepository implements RepositoryInterface
{
    abstract function getQuery(): Builder;

    public function getAll(): array
    {
        return $this->getQuery()->orderBy('id', 'asc')->get()->all();
    }

    /**
     * @param int $id
     * @return BaseModel
     * @throws ResourceNotFoundException
     */
    public function getById(int $id): BaseModel
    {
        $resource = $this->getQuery()->where('id', $id)->first();
        return $this->returnOrThrowOnNull($resource, $id);
    }

    /**
     * @param BaseModel|null $resource
     * @param int $id
     * @return BaseModel
     * @throws ResourceNotFoundException
     */
    protected function returnOrThrowOnNull(?BaseModel $resource, int $id): BaseModel
    {
        if (!$resource) {
            throw new ResourceNotFoundException(sprintf('Resource with id: %d not found', $id));
        }

        return $resource;
    }

    /**
     * @param int $id
     * @throws ResourceNotFoundException
     * @throws Exception
     */
    public function deleteById(int $id): void
    {
        $book = $this->getById($id);
        $book->delete();
    }
}
