<?php

namespace App\Repositories;

use App\Exceptions\ResourceNotFoundException;
use App\Http\Requests\BookRequest;
use App\Models\BaseModel;
use App\Models\Book;
use Illuminate\Database\Eloquent\Builder;
use Throwable;

class BookRepository extends AbstractRepository
{
    public function getQuery(): Builder
    {
        return Book::query();
    }

    /**
     * @param int $id
     * @return Book
     * @throws ResourceNotFoundException
     */
    public function getById(int $id): BaseModel
    {
        $book = $this->getQuery()->with('categories')->where('id', $id)->first();
        return $this->returnOrThrowOnNull($book, $id);
    }

    /**
     * @param BookRequest $bookRequest
     * @param int|null $id
     * @return Book
     * @throws ResourceNotFoundException
     * @throws Throwable
     */
    public function save(BookRequest $bookRequest, int $id = null): BaseModel
    {
        if ($id) {
            $book = $this->getById($id);
        } else {
            $book = new Book();
        }
        $book->title = $bookRequest->title;
        $book->saveOrFail();
        $book->categories()->sync($bookRequest->categories);
        return $book;
    }
}
