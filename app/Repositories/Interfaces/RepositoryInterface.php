<?php

namespace App\Repositories\Interfaces;

use App\Models\BaseModel;

interface RepositoryInterface
{
    public function getAll(): array;

    public function getById(int $id): BaseModel;

    public function deleteById(int $id): void;
}
