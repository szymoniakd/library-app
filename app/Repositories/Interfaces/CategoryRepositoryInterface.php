<?php

namespace App\Repositories\Interfaces;

use App\Models\Category;
use App\Models\BaseModel;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * @return Category[]
     */
    public function getAll(): array;

    /**
     * @param int $id
     * @return Category
     */
    public function getById(int $id): BaseModel;
}
