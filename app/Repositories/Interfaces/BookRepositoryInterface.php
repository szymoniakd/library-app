<?php


namespace App\Repositories\Interfaces;

use App\Models\BaseModel;
use App\Models\Book;

interface BookRepositoryInterface extends RepositoryInterface
{
    /**
     * @return Book[]
     */
    public function getAll(): array;

    /**
     * @param int $id
     * @return Book
     */
    public function getById(int $id): BaseModel;
}
