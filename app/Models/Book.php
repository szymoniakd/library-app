<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property int $id
 * @property string $title
 * @property DateTime $created_at
 * @property DateTime $updated_at
 *
 * @property Category[] $categories
 */
class Book extends BaseModel
{
    use HasFactory;

    protected $visible = ['id', 'title', 'categories'];

    protected $fillable = ['title'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
