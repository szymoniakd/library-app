<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $repository;

    public function __construct(
        RepositoryInterface $repository
    )
    {
        $this->repository = $repository;
    }

    public function index(): JsonResponse
    {
        return $this->return([
            'results' => $this->repository->getAll(),
        ]);
    }

    public function get(int $id): JsonResponse
    {
        return $this->return($this->repository->getById($id));
    }

    public function delete(int $id): JsonResponse
    {
        $this->repository->deleteById($id);
        return $this->deletedMessage('deleted');
    }

    protected function return($data): JsonResponse
    {
        return response()->json($data);
    }

    protected function createdMessage(string $message = 'created'): JsonResponse
    {
        return $this->created($this->returnMessage($message));
    }

    protected function created(array $data = ['message' => 'created']): JsonResponse
    {
        return response()->json($data, 201);
    }

    protected function updatedMessage(string $message = 'updated'): JsonResponse
    {
        return $this->updated($this->returnMessage($message));
    }

    protected function updated(array $data = ['message' => 'updated']): JsonResponse
    {
        return response()->json($data, 200);
    }

    protected function deletedMessage(string $message = 'deleted'): JsonResponse
    {
        return response()->json($this->returnMessage($message), 200);
    }

    protected function deleted(array $data = ['message' => 'deleted']): JsonResponse
    {
        return response()->json($data, 200);
    }

    private function returnMessage(string $message): array
    {
        return ['message' => $message];
    }
}
