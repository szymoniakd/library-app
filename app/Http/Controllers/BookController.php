<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookRequest;
use Illuminate\Http\JsonResponse;

class BookController extends Controller
{
    public function create(BookRequest $bookRequest): JsonResponse
    {
        $this->repository->save($bookRequest);
        return $this->createdMessage('Book created');
    }

    public function update(BookRequest $bookRequest, int $id): JsonResponse
    {
        $this->repository->save($bookRequest, $id);
        return $this->updatedMessage('Book updated');
    }
}
