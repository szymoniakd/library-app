<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    public function create(CategoryRequest $categoryRequest): JsonResponse
    {
        $this->repository->save($categoryRequest);
        return $this->createdMessage('Category created');
    }

    public function update(CategoryRequest $categoryRequest, int $id): JsonResponse
    {
        $this->repository->save($categoryRequest, $id);
        return $this->updatedMessage('Category updated');
    }
}
