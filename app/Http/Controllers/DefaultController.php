<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class DefaultController extends Controller
{
    public function __construct()
    {

    }

    public function test(): JsonResponse
    {
        return $this->return(['message' => 'library app']);
    }
}
