<?php

namespace App\Http\Requests;

/**
 * @property string $title
 * @property array $categories
 */
class BookRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'title' => 'required|max:255',
            'categories' => 'nullable|array',
        ];
    }
}
