<?php

namespace App\Http\Requests;

/**
 * @property string $name
 */
class CategoryRequest extends ApiRequest
{
    public function rules(): array
    {
        return [
            'name' => 'required|max:255'
        ];
    }
}
