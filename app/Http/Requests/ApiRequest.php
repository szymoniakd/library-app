<?php

namespace App\Http\Requests;

use App\Exceptions\RequestValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ApiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * @param Validator $validator
     * @throws RequestValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new RequestValidationException($validator->getMessageBag());
    }
}
