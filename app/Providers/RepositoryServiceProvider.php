<?php

namespace App\Providers;

use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Repositories\BookRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\Interfaces\BookRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\RepositoryInterface;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(BookRepositoryInterface::class, BookRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);

        $this->app->when(BookController::class)->needs(RepositoryInterface::class)->give(BookRepository::class);
        $this->app->when(CategoryController::class)->needs(RepositoryInterface::class)->give(CategoryRepository::class);
    }
}
