# Library abb

Another CRUD app with books and categories. Built with Laravel + Angular.

## Installation

Make sure you have Vagrant, then set in Homestead.yaml _folders - map:_ path to host machine projects root.

```
vagrant up
```

For convenience add to host machine hosts:
```
192.168.10.10 library.test
192.168.10.10 api.library.test
```
App will be available on [library.test](http://library.test).


### Building frontend
On host machine run
```
cd frontend && npm install && npm run build
```

## Tests
Go to guest machine with SSH:

```
vagrant ssh
```

and go to app directory to run tests:

```
cd /home/vagrant/code/
php artisan tests
```

# Zadanie 2
```
vagrant ssh
cd /home/vagrant/code
php artisan test:structure
```
