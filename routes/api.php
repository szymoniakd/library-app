<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DefaultController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', [DefaultController::class, 'test']);

Route::prefix('/book')->group(function() {
    Route::get('/', [BookController::class, 'index']);
    Route::post('/', [BookController::class, 'create']);

    Route::prefix('/{id}')->group(function() {
        Route::get('/', [BookController::class, 'get']);
        Route::put('/', [BookController::class, 'update']);
        Route::delete('/', [BookController::class, 'delete']);
    });
});

Route::prefix('/category')->group(function() {
    Route::get('/', [CategoryController::class, 'index']);
    Route::post('/', [CategoryController::class, 'create']);

    Route::prefix('/{id}')->group(function() {
        Route::get('/', [CategoryController::class, 'get']);
        Route::put('/', [CategoryController::class, 'update']);
        Route::delete('/', [CategoryController::class, 'delete']);
    });
});
