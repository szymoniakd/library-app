<?php

namespace Tests\Feature;

use App\Models\Book;
use App\Models\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BooksTest extends TestCase
{
    use WithFaker;

    public function testBooksFetching(): void
    {
        $response = $this->get('/book');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'results' => [],
        ]);
        $data = $response->json();
        $this->assertArrayHasKey('id', $data['results'][0]);
        $this->assertArrayHasKey('title', $data['results'][0]);
        $this->assertArrayNotHasKey('categories', $data['results'][0], 'book list should not contain details');
    }

    public function testBookCreatingWithoutCategories(): void
    {
        $response = $this->post('/book', [
            'title' => $this->faker->catchPhrase,
        ]);
        $response->assertStatus(201);
        $response->assertJson(['message' => 'Book created']);
    }

    public function testBookCreatingWithoutTitle(): void
    {
        $response = $this->post('/book', [
            'title' => '',
        ]);
        $response->assertStatus(400);
        $response->assertJsonStructure(['message', 'errors' => [
            'title'
        ]]);
    }

    public function testBookCreatingWithCategories(): void
    {
        $response = $this->post('/book', [
            'title' => $this->faker->catchPhrase,
            'categories' => [$this->getFirstCategory()->id],
        ]);
        $response->assertStatus(201);
        $response->assertJson(['message' => 'Book created']);
    }

    public function testSingleBookFetching(): void
    {
        $title = $this->faker->catchPhrase;
        $this->post('/book', [
            'title' => $title,
        ]);
        $book = $this->getLastAddedBook();
        $this->assertEquals($title, $book->title);
        $response = $this->delete('/book/' . $book->id);
        $response->assertStatus(200);
        $response->assertJson(['message' => 'deleted']);
    }

    public function testBookUpdatingAndDetails(): void
    {
        $book = $this->getLastAddedBook();
        $newTitle = 'some new title';
        $category = $this->getFirstCategory();
        $response = $this->put('/book/' . $book->id, ['title' => $newTitle, 'categories' => [$category->id]]);
        $response->assertStatus(200);
        $book = $this->getLastAddedBook();
        $this->assertEquals($newTitle, $book->title);
        $this->assertEquals($book->categories()->allRelatedIds()->toArray(), [$category->id]);

        $response = $this->get('/book/' . $book->id);
        $response->assertJson([
            'id' => $book->id,
            'title' => $book->title,
            'categories' => [
                $category->toArray(),
            ],
        ]);
    }

    private function getFirstCategory(): Category
    {
        return Category::query()->first();
    }

    private function getLastAddedBook(): Book
    {
        return Book::query()->orderBy('id', 'desc')->first();
    }
}
